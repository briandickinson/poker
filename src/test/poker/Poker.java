package test.poker;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Poker
{

  /*
   * Given a set of 5 playing card identifiers such as 2H, 7C, QS, 10D, 2D;
   * determine if this hand is better than some other hand, according to the rules of poker.
   *
   * Hands will be a string with 5 cards comma separated,
   * each card will have 1-2 digits or JQKA and a suit indicator C,D,S,H (i.e. 10C, KH)
   *
   * Possible Hand Types Below:
   *   Straight flush
   *   Four of a kind
   *   Full house
   *   Flush
   *   Straight
   *   Three of a kind
   *   Two pair
   *   One pair
   *
   * The goal of this is to compare between the hand types.
   * Comparing 2 of the same type (i.e. 2 straights) to determine a winner is outside the scope
   * and will not be tested.
   *
   * Implement PokerHand.isGreaterThan(...) method and return whether or not the first hand wins over the second hand.
   */

   static class PokerHand {

      private String handAsString;

      public PokerHand(String hand) {
         handAsString = hand;
      }


      // Ideally I would put Card, Rank, and Suite all in separate files
      // The nested enums are clunky to read when used
       static class Card {
         private Rank rank;
         private Suite suite;

         public Rank getRank() {
            return rank;
         }

         public Suite getSuite() {
            return suite;
         }

         public Card(String cardString) {
            suite = determineSuite(cardString);
            rank = determineRank(cardString);
         }

         // Currently no exception handling for bad strings with no match
         private Suite determineSuite(String cardString) {
            String suiteStr = cardString.substring(cardString.length() - 1);
            return suiteMap.get(suiteStr);
         }

         private Rank determineRank(String cardString) {
            String rankStr;

            // Special handling for "10" since it is the only rank with two characters
            if (cardString.startsWith("10")) {
               rankStr = "10";
            }
            else {
               rankStr = cardString.substring(0,1);
            }
            return rankMap.get(rankStr);
         }

          private static HashMap<String, Rank> rankMap = new HashMap<>() {{
             put("2", Rank.TWO);
             put("3", Rank.THREE);
             put("4", Rank.FOUR);
             put("5", Rank.FIVE);
             put("6", Rank.SIX);
             put("7", Rank.SEVEN);
             put("8", Rank.EIGHT);
             put("9", Rank.NINE);
             put("10", Rank.TEN);
             put("J", Rank.JACK);
             put("Q", Rank.QUEEN);
             put("K", Rank.KING);
             put("A", Rank.ACE);
          }};

          private static HashMap<String, Suite> suiteMap = new HashMap<>() {{
             put("C", Suite.CLUBS);
             put("D", Suite.DIAMONDS);
             put("H", Suite.HEARTS);
             put("S", Suite.SPADES);
          }};

          enum Rank {
             TWO,
             THREE,
             FOUR,
             FIVE,
             SIX,
             SEVEN,
             EIGHT,
             NINE,
             TEN,
             JACK,
             QUEEN,
             KING,
             ACE
          }

          enum Suite {
             CLUBS,
             DIAMONDS,
             HEARTS,
             SPADES
          }
       }

      enum TypeOfHand {
         HIGHEST_CARD,
         ONE_PAIR,
         TWO_PAIR,
         THREE_OF_A_KIND,
         STRAIGHT,
         FLUSH,
         FULL_HOUSE,
         FOUR_OF_A_KIND,
         STRAIGHT_FLUSH
      }

      TreeMap<Card.Rank, Integer> sortByRank(List<Card> cards) {
         TreeMap<Card.Rank, Integer> rankCounts = new TreeMap<>();
         for (Card.Rank rank : Card.Rank.values()) {
            rankCounts.put(rank, 0);
         }

         // Initialize map with each Rank and zero count
         // Each Rank must be included in order to use the algorithm for detecting straights
         // Not sure that streams are better way of doing this - see code above
//         TreeMap<Card.Rank, Integer> rankCounts = Arrays.stream(Card.Rank.values())
//                 .collect(Collectors.toMap(
//                                 Function.identity(),
//                                 i -> 0,
//                                 (a,b) -> { throw new RuntimeException("Duplicate enums not possible!"); },
//                                 TreeMap::new));

         for (Card card : cards) {
            Integer currentRankCount = rankCounts.get(card.getRank());
            rankCounts.put(card.getRank(), currentRankCount + 1);
         }
         return rankCounts;
      }

      Map<Card.Suite, Integer> sortBySuite(List<Card> cards) {
         // Initialize map with each Suite and zero count
         Map<Card.Suite, Integer> suiteCounts = Arrays.stream(Card.Suite.values())
                 .collect(Collectors.toMap(Function.identity(), i -> 0));

         for (Card card : cards) {
            Integer currentSuiteCount = suiteCounts.get(card.getSuite());
            suiteCounts.put(card.getSuite(), currentSuiteCount + 1);
         }
         return suiteCounts;
      }

      boolean checkForFlush(List<Card> cards) {
         boolean isFlush = false;
         Map<Card.Suite, Integer> suiteCounts = sortBySuite(cards);
         for (Map.Entry<Card.Suite,Integer> suiteAndCount : suiteCounts.entrySet()) {
            if (suiteAndCount.getValue().equals(5)) {
               isFlush = true;
               break;
            }
         }
         return isFlush;
      }

      boolean checkForStraight(TreeMap<Card.Rank,Integer> sortedRankCounts) {
         boolean isStraight = false;
         int straightCount = 0;
         for (Map.Entry<Card.Rank,Integer> rankAndCount : sortedRankCounts.entrySet()) {
            // Only possible if there is one and only one of each card
            // If there is a missing card or more than one then straightCount will reset
            if (rankAndCount.getValue().equals(1)) {
               straightCount++;
               if (straightCount == 5) {
                  isStraight = true;
                  break;
               }
            }
            else {
               straightCount = 0;
            }
         }
         return isStraight;
      }

      Integer findDuplicates(Map<Card.Rank, Integer> rankCounts, Integer threshold) {
         Integer foundDuplicates;
         Stream<Map.Entry<Card.Rank, Integer>> entries = rankCounts.entrySet().stream();
         List duplicates =
                 entries
                         .filter(count -> count.getValue().equals(threshold))
                         .collect(Collectors.toList());

         foundDuplicates = duplicates.size();
         return foundDuplicates;
      }

      TypeOfHand determineTypeOfHand(List<Card> cards) {
         TypeOfHand typeOfHand = TypeOfHand.HIGHEST_CARD;

         TreeMap<Card.Rank, Integer> rankCounts = sortByRank(cards);

         Boolean isFlush = checkForFlush(cards);
         Boolean isStraight = checkForStraight(rankCounts);
         Integer foundPairs = findDuplicates(rankCounts, 2);
         Boolean foundThreeOfAKind = findDuplicates(rankCounts, 3) > 0 ? true : false;
         Boolean foundFourOfAKind = findDuplicates(rankCounts, 4) > 0 ? true : false;

         if (isStraight && isFlush) {
            typeOfHand = TypeOfHand.STRAIGHT_FLUSH;
         }
         else if (foundFourOfAKind) {
            typeOfHand = TypeOfHand.FOUR_OF_A_KIND;
         }
         else if (foundPairs == 1 && foundThreeOfAKind) {
            typeOfHand = TypeOfHand.FULL_HOUSE;
         }
         else if ( ! isStraight && isFlush) {
            typeOfHand = TypeOfHand.FLUSH;
         }
         else if (isStraight && ! isFlush) {
            typeOfHand = TypeOfHand.STRAIGHT;
         }
         else if (foundPairs == 0 && foundThreeOfAKind) {
            typeOfHand = TypeOfHand.THREE_OF_A_KIND;
         }
         else if (foundPairs == 2) {
            typeOfHand = TypeOfHand.TWO_PAIR;
         }
         else if (foundPairs == 1 && ! foundThreeOfAKind) {
            typeOfHand = TypeOfHand.ONE_PAIR;
         }
         return typeOfHand;
      }

      List<Card> getCards() {
         List<Card> cards = new ArrayList<>();
         String[] cardStrings = handAsString.split(",");
         for (String cardString : cardStrings) {
            Card card = new Card(cardString);
            cards.add(card);
         }
         return cards;
      }

      // If comparing hands of the same type, I might use an interface HandType.
      // HandType could contain a method for comparing to another of the same HandType
      // With a separate implementation for each (an implementation for Straight, a
      // different one for Three-of-a-kind, etc).
      // HandType could also contain a method something like getHandTypeRank() that would
      // return a value similar to the current TypeOfHand enum ordinal, for easy comparison
      // of different hand types.
      /**
       * Determines whether this hand's type is better than another hand's type
       * If they are the same type, it does not do any further comparison
       * @param hand2
       * @return
       */
      public Boolean isGreaterThan(PokerHand hand2) {
         List<Card> thisHandCards = getCards();
         List<Card> otherHandCards = hand2.getCards();
         TypeOfHand thisHandType = determineTypeOfHand(thisHandCards);
         TypeOfHand otherHandType = determineTypeOfHand(otherHandCards);

         return thisHandType.ordinal() > otherHandType.ordinal();
      }

      @Override
      public String toString() {
         return handAsString;
      }
   }

   public static void testHand1IsGreaterThanHand2(String hand1AsString,
                                                  String hand2AsString,
                                                  Boolean expectedResult) {
      PokerHand hand1 = new PokerHand(hand1AsString);
      PokerHand hand2 = new PokerHand(hand2AsString);
      System.out.println("Hand1[" + hand1 + "] > Hand2[" + hand2 + "] \t-- " +
                         "expected: " + expectedResult + ", actual: " + hand1.isGreaterThan(hand2));
   }

   public static void main(String[] args) {
      testHand1IsGreaterThanHand2(
              "8C,9C,10C,JC,QC", // straight flush
              "6S,7H,8D,9H,10D",
              true);

      testHand1IsGreaterThanHand2(
              "4H,4D,4C,4S,JS", //four of a kind
              "6C,6S,KH,AS,AD",
              true);

      testHand1IsGreaterThanHand2(
              "6C,6D,6H,9C,KD",
              "5C,3C,10C,KC,7C", // flush
              false);

      testHand1IsGreaterThanHand2(
              "4H,4D,4C,KC,KD", // full house
              "9D,6S,KH,AS,AD",
              true);

      testHand1IsGreaterThanHand2(
              "6C,6D,6H,9C,KD",
              "2C,3C,4S,5S,6S", // straight
              false);

      testHand1IsGreaterThanHand2(
              "7C,7D,7S,3H,4D", // three of a kind
              "9S,6S,10D,AS,AD",
              true);

      testHand1IsGreaterThanHand2(
              "2S,2D,JH,7S,AC",
              "8C,8H,10S,KH,KS", // two pair
              false);

      testHand1IsGreaterThanHand2(
              "AC,AH,3C,QH,10C", // one pair
              "3S,2D,KH,JS,AD",
              true);
   }
}


